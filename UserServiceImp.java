/**
 * 
 */
package com.amdocs.user.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amdocs.user.dao.UserDao;

@Service
public class UserServiceImp implements UserDetailsService ,UserService {

	@Autowired
	private UserDao userDao;
	
	@Transactional
	@Override
	public UserDetails loadUserByUsername(String arg0)
			throws UsernameNotFoundException {
		boolean accountNonExpired=true;
		boolean credentialsNonExpired=true;
		boolean accountNonLocked=true;
		
		List<SimpleGrantedAuthority> authorities=new ArrayList<SimpleGrantedAuthority>();
		
		com.amdocs.user.model.User user=userDao.getUser(arg0);
		
		
		SimpleGrantedAuthority roleAdmin=new SimpleGrantedAuthority(user.getRole());
		authorities.add(roleAdmin);
		
		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
	}

	@Transactional
	@Override
	public com.amdocs.user.model.User getUser(String username) {
		return userDao.getUser(username);
	}

	

}
