/**
 * 
 */
package com.amdocs.user.service;

import com.amdocs.user.model.User;


public interface UserService {
	User getUser(String username);
}
