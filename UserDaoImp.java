/**
 * 
 */
package com.amdocs.user.dao;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.amdocs.user.model.User;
import com.amdocs.user.model.UserMapper;

@Repository
public class UserDaoImp implements UserDao {

	@Autowired
	private DataSource dataSource;
	
	@Override
	public User getUser(String username) {
		JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
		String sql="select * from [user] where [username]=?";
		User user=(User)jdbcTemplate.queryForObject(sql,new Object[]{
				username
		},new UserMapper());
		return user;
	}

}

