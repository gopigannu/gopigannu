/**
 * 
 */
package com.amdocs.user.controller;

import java.security.GeneralSecurityException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amdocs.user.service.UserService;


@Controller
public class SignInSingOutController {
	
	private static Logger logger=LoggerFactory.getLogger(SignInSingOutController.class);
	
	@Autowired
	private UserService userService;
	
	
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String signin(Locale locale, Model model) throws GeneralSecurityException {
		

		return "signin";
	}
	
	@RequestMapping(value = "/signinfail", method = RequestMethod.GET)
	public String signinfail(Locale locale, Model model) {
		

		return "signin";
	}


}
