/**
 * 
 */
package com.amdocs.common.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class FailureAuthenticationHandler implements
		AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authenticationException)
			throws IOException, ServletException {
		//authenticationException.printStackTrace();
		System.out.println("Authetication fail"+authenticationException.getMessage());
		response.sendRedirect("signinfail?error=1");

	}

}
