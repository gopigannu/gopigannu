/**
 * 
 */
package com.amdocs.user.dao;

import com.amdocs.user.model.User;


public interface UserDao {

	User getUser(String username);
	
}
